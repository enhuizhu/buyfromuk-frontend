import React, { Component } from 'react';
import Header from './components/Header/Header'
import Notifications from './components/Notifications/Notifications';
import CssBaseline from '@material-ui/core/CssBaseline';
import Carousel from './components/Carousel/Carousel';
import { getCategories } from './actions/category.action';
import { connect } from 'react-redux';
import './App.css';
import ApiService from './services/api.services';

class App extends Component {
  constructor() {
    super();
    
    this.state = {
      countries: [
        {
          name: 'en',
          active: true
        },
        {
          name: 'cn',
          active: false
        }
      ],
      categoryObj: null
    }
  }

  componentDidMount() {
    ApiService.getCategories().then(results => {
      this.setState({
        categoryObj: this.groupCategory(results.data)
      })
    }).catch(e => {
      console.error(e);
    });
  }

  groupCategory(flatCategories) {
    // find the top category
    flatCategories.forEach(category => {
      // find its parent
      const parent = flatCategories.find((c) => {
        return c.id === category.parent
      });

      if (parent) {
        if (!parent.children) {
          parent.children = [];
        }

        parent.children.push(category);
      }
    });

    // find the top category
    return flatCategories.find(c => {
      return c.parent === null;
    });
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline/>
        <div className="App">
          <Header countries={this.state.countries}></Header>
          <Carousel></Carousel>
          <Notifications></Notifications>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
// const mapDispatchToProps = (dispatch) => {
//   return {
//     getCategories: () => {
//       dispatch(getCategories())
//     }
//   }
// };

// export default connect(null, mapDispatchToProps)(App);
