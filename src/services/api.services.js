import { config } from '../config';
import axios from 'axios';
import store from '../store/store';

export default class ApiService {
  static login(userInfo) {
    return axios.post(this.getUrl('api-token-auth'), userInfo)
  }

  static getUserProfile() {
    return axios.get(this.getUrl('account/userProfile'), this.getHeaderWithToken());
  }

  static getCategories() {
    return axios.get(this.getUrl('categories'));
  }

  static getUrl(path) {
    return `${config.api}/${path}/`;
  }

  static getHeaderWithToken() {
    const state = store.getState();
    const token = state.userInfo.token;

    if (!token) {
      throw new Error('please login first!');
    }

    return  {
      headers: {'AUTHORIZATION': `JWT ${token}`}
    }
  }
}