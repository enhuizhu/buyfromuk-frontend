import { LOGIN, RECEIVE_USER_INFO, LOGOUT } from '../actions/user.action';

const USER_TOKEN = 'userToken';
const USER_PROFILE = 'userProfile';

var sessionStorage = window.sessionStorage || 
  {
    getItem: () => {
      return null;
    },

    setItem: () => {

    }, 

    removeItem: () => {

    },
  };

const getUserProfileFromSessionStore = () => {
  const profile = sessionStorage.getItem(USER_PROFILE);

  if (profile) {
    return JSON.parse(profile);
  }

  return {};
}

const defaultState = {
  token: sessionStorage.getItem(USER_TOKEN),
  profile: getUserProfileFromSessionStore(),
};

export const userInfo = (state = defaultState, action) => {
  switch(action.type) {
    case LOGIN:
      sessionStorage.setItem(USER_TOKEN, action.payload.token);
      return Object.assign({}, state, action.payload);
    case RECEIVE_USER_INFO:
      sessionStorage.setItem(USER_PROFILE, JSON.stringify(action.payload));
      return Object.assign({}, state, {profile: action.payload});
    case LOGOUT:
      sessionStorage.removeItem(USER_TOKEN);
      sessionStorage.removeItem(USER_PROFILE);
      return {
        token: null,
        profile: {},
      }
    default:
      return defaultState;
  }
};
