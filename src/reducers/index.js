import { combineReducers } from 'redux';
import { userInfo } from '../reducers/userInfo.reducer';
import { notification } from '../reducers/notification.reducer';

export default combineReducers({
  userInfo,
  notification
});