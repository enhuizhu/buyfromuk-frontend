import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import { loginSync, getUserProfile } from '../../actions/user.action';
import ApiService from '../../services/api.services';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  
  TextField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,    
  },

  paper: {
    position: 'absolute',
    width: 280,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },

  btn: {
    marginTop: 10,
    marginLeft: 10,
  },

  loader: {
    marginTop: 10,
    marginLeft: 10
  }
});

class LoginForm extends React.Component {
  constructor() {
    super();
    
    this.state = {
      username: '',
      pass: '',
      isLoading: false,
      loginError: false
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    }); 
  }

  handleClose = () => {
    this.props.closeCallback();
  }

  login = () => {
    console.log('username', this.state.username);
    console.log('password', this.state.pass);
    
    this.setState({
      isLoading: true
    });

    ApiService.login({
      username: this.state.username,
      password: this.state.pass
    }).then(res => {
      console.log('res', res);
      this.props.login(res.data);
      this.props.getUserProfile();
    }).catch(e => {
      console.error('error', e);
      this.setState({
        loginError: true,
        isLoading: false
      });
    });
  } 

  render() {
    const { classes } = this.props;
    
    return (
      <div>
        <Modal 
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.props.open}
          onClose={this.handleClose}
        >
          <Paper style={getModalStyle()} className={classes.paper}>
            <form className={classes.container}>
              <TextField
                error={this.state.loginError}
                label="User Name"
                value={this.state.username}
                className={classes.TextField}
                onChange={this.handleChange('username')}                
              />
              <TextField
                error={this.state.loginError}
                label="User Password"
                value={this.state.pass}
                className={classes.TextField}
                onChange={this.handleChange('pass')}
                type="password"
              />
              { 
                this.state.isLoading ?  
                <CircularProgress
                  variant="indeterminate"
                  disableShrink
                  className={classes.loader}
                  size={24}
                  thickness={4}
                /> : 
                <Button 
                  variant="contained" 
                  color="primary"
                  className={classes.btn}
                  onClick={this.login}
                  type="submit"
                >
                  Login
                </Button>
              }
            </form>
          </Paper>
        </Modal>
      </div>
    );
  }
};

LoginForm.propTypes = {
  classes: PropTypes.object.isRequired,
  closeCallback: PropTypes.func,
  open: PropTypes.bool
};

LoginForm.defaultProps = {
  open: false
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (userInfo) => {
      dispatch(loginSync(userInfo));
    },

    getUserProfile: () => {
      dispatch(getUserProfile());
    }
  };
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(LoginForm))
