import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import SnackContent from './SnackContent';
import PropTypes from 'prop-types';
import { removeNotification } from '../../actions/notification.action'
import { ERROR } from '../../constants/notification.constant';
import { connect } from 'react-redux';

const styles = theme => ({
  close: {
    padding: theme.spacing.unit / 2,
  },
});


class Notifications extends React.Component {
  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.props.removeNotification();
  };

  render() {
    if (!this.props.notification) {
      return '';
    }

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={Boolean(this.props.notification)}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackContent
            onClose={this.handleClose}
            variant={this.props.notification.type}
            message={this.props.notification.msg}
          />
        </Snackbar>
      </div>
    );
  }
}

Notifications.propTypes = {
  notification: PropTypes.object
};

const mapStateToProps = (state) => {
  return {
    notification: state.notification
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    removeNotification: () => {
      dispatch(removeNotification());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Notifications));
