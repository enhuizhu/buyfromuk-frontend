import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from '@material-ui/core/styles';
import { SUCCESS, WARNING, ERROR, INFO } from '../../constants/notification.constant';

const variantIcon = {
  [SUCCESS]: 'check_circle ',
  [WARNING]: 'warning ',
  [ERROR]: 'error ',
  [INFO]: 'info ',
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function SnackContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const iconType = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)}>{iconType}</Icon>
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <Icon className={classes.icon}>close</Icon>
        </IconButton>,
      ]}
      {...other}
    />
  );
}

SnackContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf([ SUCCESS, WARNING, ERROR, INFO ]).isRequired,
};

export default withStyles(styles1)(SnackContent);
