import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import LoginForm from '../LoginForm/LoginForm';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux';
import { logout } from '../../actions/user.action';
import { receiveNotification } from '../../actions/notification.action'
import { SUCCESS } from '../../constants/notification.constant';
// import ApiService from '../../services/api.services';

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: '24px 12px'
  },
  paper: {
    padding: theme.spacing.unit * 4,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  logoText: {
    fontSize: 35,
    fontStyle: 'italic',
    textShadow: '1px 1px 1px grey',
    [theme.breakpoints.down('xs')]: {
      fontSize: 25
    }
  },
  loginForm: {
    float: 'right',
  },
  icon: {
    color: theme.palette.text.secondary,
    cursor: 'pointer',
    float: 'right'
  },
  label: {
    marginLeft: 5,
    verticalAlign: 'super' 
  }
});

class Header extends React.Component {
  constructor() {
    super();
    
    this.state ={
      open: false,
      anchorEl: null,
    }
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleSelect = (operation) => {
    return () => {
      this.setState({ anchorEl: null });
      
      switch(operation) {
        case 'logout':
          this.setState({open: false});
          this.props.logout();
          return ;
      }
    }
  }

  render() {
    const { classes } = this.props;
    const { anchorEl } = this.state;

    const profileWrapper = (
      <React.Fragment>
        <div 
          className={classes.icon}  
          aria-owns={anchorEl ? 'profile-menu' : undefined}
          aria-haspopup="true"
          onClick={this.handleClick}>
          <Icon>person</Icon> 
          <label className={classes.label}>{this.props.userProfile.username}</label>
        </div>
        <Menu
          id="profile-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleSelect('account')}>My account</MenuItem>
          <MenuItem onClick={this.handleSelect('logout')}>Logout</MenuItem>
        </Menu>
      </React.Fragment>
    );

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={6}>
            <div className={classes.logoText}>Buy From Uk</div>
          </Grid>
          <Grid item xs={6}>
            {
              this.props.isLogged ?
                profileWrapper :
                <div className={classes.loginForm}>
                  you have not login yet, 
                  <Button 
                    variant="contained" 
                    color="primary"
                    onClick={() => {
                      this.setState({
                        open: true
                      });
                    }}
                  >Login</Button>?
                </div>
            }
          </Grid>
        </Grid>
        {
          this.props.isLogged ? 
            '' : 
            <LoginForm open={this.state.open} closeCallback={() => {
              this.setState({
                open: false
              });
            }}/> 
        }
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  return {
    isLogged: !!state.userInfo.token,
    userProfile: state.userInfo.profile
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      dispatch(logout());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Header));
