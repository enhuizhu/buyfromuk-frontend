import { errorHandler } from '../helpers/error.helper';
import ApiService from '../services/api.services';

export const LOGIN = 'LOGIN';
export const LOGOUT =  'LOGOUT';
export const RECEIVE_USER_INFO = 'RECEIVE_USER_INFO';

export const loginSync = (userInfo) => {
  return {
    type: LOGIN,
    payload: userInfo
  };
}

export const logout = () => {
  return {
    type: LOGOUT
  }
};

export const getUserProfile = () => {
  return (dispatch) => {
    ApiService.getUserProfile().then(res => {
      console.log('profile', res);
      if (res.data.error) {
        errorHandler(res.data);
      } else {
        dispatch({
          type: RECEIVE_USER_INFO,
          payload: res.data.data 
        });
      }
    }).catch(errorHandler);
  };
};
