export const RECEIVE_NOTIFICATION = 'RECEIVE_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

export const receiveNotification = (notificationType, msg) => {
  return {
    type: RECEIVE_NOTIFICATION,
    payload: {
      type: notificationType,
      msg: msg
    }
  }
}

export const removeNotification = () => {
  return {
    type: REMOVE_NOTIFICATION
  }
};