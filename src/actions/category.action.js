import { errorHandler } from '../helpers/error.helper';
import ApiService from '../services/api.services';

export const GET_CATEGORIES = 'GET_CATEGORIES';
export const RECEIVE_CATEGORIES = 'RECEIVE_CATEGORIES';

export const getCategories = () => {
  return (dispatch) => {
    ApiService.getCategories().then(results => {
      console.log(results);
    }).catch(errorHandler);
  }
}
