// jest.enableAutomock()

import React from 'react';
import App from './App';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ApiService from './services/api.services';

const sinon = require('sinon');

configure({ adapter: new Adapter() });

describe('test main app', () => {
  sinon.stub(ApiService, 'getCategories').callsFake(() => {
    return new Promise((resolve, reject) => {
      resolve({
        data: [
          {name: "Baby Food", parent: null, id: 1},
          {name: "Baby Milk", parent: 1, id: 2}
        ]
      });
    });
  });

  it('testing group function', () => {
    const app = shallow(<App></App>);
    const instance = app.instance();
    const data = [
      {name: "Baby Food", parent: null, id: 1},
      {name: "Baby Milk", parent: 1, id: 2}
    ];
    
    expect(instance.groupCategory(data)).toEqual({
      name: "Baby Food", 
      parent: null, id: 1,
      children: [
        {
          name: "Baby Milk", 
          parent: 1, 
          id: 2
        }
      ]
    });
  });
});