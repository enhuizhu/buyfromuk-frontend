import store from './store';
import { LOGIN, RECEIVE_USER_INFO, logout } from '../actions/user.action';
import { receiveNotification, removeNotification} from '../actions/notification.action';
import { WARNING, SUCCESS, ERROR, INFO } from '../constants/notification.constant';

describe('test store', () => {
  it('test store state', () => {
    let state = store.getState();
    expect(state.userInfo.token).toBeNull();

    store.dispatch({type: LOGIN, payload: {token: 'test'}});
    state = store.getState();
    expect(state.userInfo.token).toBe('test');
    store.dispatch({type: RECEIVE_USER_INFO, payload: {username: 'test', email: 'test@test.com'}});
    state = store.getState();
    expect(state.userInfo.profile.email).toBe('test@test.com');
  });

  it('test notification state', () => {
    let state = store.getState();
    expect(state.notification).toBeNull();
    store.dispatch(receiveNotification(WARNING, 'test'));
    state = store.getState();
    expect(state.notification).toEqual({ type: 'warning', msg: 'test' });
    store.dispatch(removeNotification());
    state = store.getState();
    expect(state.notification).toBeNull();
  });

  it('test logout state', () => {
    let state = store.getState();
    store.dispatch(logout());
    state = store.getState();
    expect(state.userInfo).toEqual({ token: null, profile: {}});
  });
});